<?php

/**
 * @file
 * Admin file for Elasticsearch Consumer
 */


/**
 * Page callback that shows an overview of defined consumers.
 */
function elasticsearch_consumer_admin_settings() {
  return drupal_get_form("elasticsearch_consumer_admin_settings_form");
}


/**
 * Form settings callback
 */
function elasticsearch_consumer_admin_settings_form($form, &$form_state) {
  $form = elasticsearch_consumer_search_admin();
  return system_settings_form($form["elasticsearch_consumer"]);
}
