<?php

/**
 * @file
 * Date query type plugin for the Apache Solr adapter.
 */

/**
 * Plugin for "date" query types.
 */
class  ElasticsearchFacetapiDate extends FacetapiQueryTypeDate implements FacetapiQueryTypeInterface {

  /**
   * Returns the query type associated with the plugin.
   *
   * @return string
   *   The query type.
   */
  static public function getType() {
    return 'date';
  }

  /**
   * Adds the facet & filter to the query object.
   *
   * @param ElasticsearchConsumerQuery $query
   *   An object containing the query in the backend's native API.
   */
  public function execute($query) {
    $settings = $this->getSettings();
    $interval_end = empty($settings->settings['facet_date_interval_end']) ? FACETAPI_DATE_YEAR : $settings->settings['facet_date_interval_end'];

    // add the facet
    $facet = new \Elastica\Facet\DateHistogram($this->facet['name']);
    $facet->setField($this->facet['field']);
    $facet->setInterval(strtolower($interval_end));
    $query->addFacet($facet);

    // add the filter
    $operator = $settings->settings['operator'];
    $active_items = $this->getActiveItems($this->facet);
    if($active_items) {
      $class = $operator == FACETAPI_OPERATOR_OR ? 'BoolOr' : 'BoolAnd';
      $class = '\\Elastica\\Filter\\'.$class;
      $filterBool = new $class();
      foreach ($active_items as $key => $item) {
        $filter  = new \Elastica\Filter\Range($this->facet['name'], array(
          'from' =>  $item['start'],
          'to' => $item['end']
        ));
        $filterBool->addFilter($filter);
      }

      // add filter to the query, according to the configuration
      $query->addFilter($filterBool, !empty($settings->settings['facet_calculation_post_query']));
    }
  }

  /**
   * Initializes the facet's build array.
   *
   * @return array
   *   The initialized render array.
   */
  public function build() {
    $build = array();
    $name = $this->facet["name"];
    $settings = $this->getSettings();
    $interval_start = empty($settings->settings['facet_date_interval_start']) ? FACETAPI_DATE_YEAR : $settings->settings['facet_date_interval_start'];
    $interval_end = empty($settings->settings['facet_date_interval_end']) ? FACETAPI_DATE_YEAR : $settings->settings['facet_date_interval_end'];
    $resultset = $this->adapter->getQuery()->getResultset();
    if($resultset) {
      $facets = $resultset->getFacets();
      if(isset($facets[$name])) {
        foreach($facets[$name]['entries'] as $entry) {
          $timestamp = $entry['time']/1000;
          $interval = false;
          $parent_label = false;
          while ($interval <> $interval_end && $interval <> FACETAPI_DATE_SECOND) {
            if($interval === false) {
              $interval = $interval_start;
            } else {
              $interval = facetapi_get_next_date_gap($interval);
            }
            // label
            $start = facetapi_isodate($timestamp, $interval);
            $end = facetapi_get_next_date_increment($start, $interval);
            $label = "[$start TO $end]";
            // count
            if(isset($build[$label])) {
              $build[$label]['#count'] += $entry['count'];
            } else {
              $build[$label] = array(
                "#count" => $entry['count']
              );
            }
            // hierarchy
            if($parent_label) {
              $build[$parent_label]['#item_children'][$label] = &$build[$label];
              $build[$label]['#item_parents'][$parent_label] = $parent_label;
            }
            $parent_label = $label;
          }
        }
      }
    }
    return $build;
  }
}
