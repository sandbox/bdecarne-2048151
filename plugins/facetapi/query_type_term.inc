<?php

/**
 * @file
 * Term query type plugin for the Example Faceted Navigation  adapter.
 */

/**
 * Plugin for "term" query types.
 */
class ElasticsearchFacetapiTerm extends FacetapiQueryType implements FacetapiQueryTypeInterface {

  /**
   * Returns the query type associated with the plugin.
   *
   * @return string
   *   The query type.
   */
  static public function getType() {
    return 'term';
  }

  /**
   * Adds the facet & filter to the query object.
   *
   * @param ElasticsearchConsumerQuery $query
   *   An object containing the query in the backend's native API.
   */
  public function execute($query) {
    $settings = $this->getSettings();
    $limit = empty($settings->settings['hard_limit']) ? 20 : (int) $settings->settings['hard_limit'];

    // add the facet
    $facet = new \Elastica\Facet\Terms($this->facet['name']);
    $facet->setField($this->facet['field']);
    $facet->setSize($limit > 0 ? $limit : 99999);
    $query->addFacet($facet);

    // add the filter
    $operator = $settings->settings['operator'];
    $active_items = $this->getActiveItems();
    if($active_items) {
      $class = $operator == FACETAPI_OPERATOR_OR ? 'BoolOr' : 'BoolAnd';
      $class = '\\Elastica\\Filter\\'.$class;
      $filterBool = new $class();
      foreach ($active_items as $key => $item) {
        $filter  = new \Elastica\Filter\Term();
        $filter->setTerm($this->facet['name'], $item['value']);
        $filterBool->addFilter($filter);
      }
      // add filter to the query, according to the configuration
      $query->addFilter($filterBool, !empty($settings->settings['facet_calculation_post_query']));
    }
  }

  /**
   * Initializes the facet's build array.
   *
   * @return array
   *   The initialized render array.
   */
  public function build() {
    $build = array();
    $name = $this->facet["name"];
    $resultset = $this->adapter->getQuery()->getResultset();
    if($resultset) {
      $facets = $resultset->getFacets();
      if(isset($facets[$name])) {
        foreach($facets[$name]['terms'] as $term) {
          $build[$term['term']] = array("#count" => $term['count']);
        }
      }
    }
    return $build;
  }
}
