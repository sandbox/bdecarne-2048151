<?php

/**
 * @file
 * Classes used by the Facet API module.
 */

/**
 * Facet API adapter example.
 */
class ElasticsearchConsumerFacetapiAdapter extends FacetapiAdapter {

  private $query;

  /**
   * @param mixed $query
   */
  public function setQuery($query) {
    $this->query = $query;
  }

  /**
   * @return mixed
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Allows the backend to initialize its query object before adding the facet
   * filters.
   *
   * @param mixed $query
   *   The backend's native object.
   */
  function initActiveFilters($query) {
    $this->setQuery($query);
  }

  /**
   * Returns a boolean flagging whether $this->searcher executed a search.
   */
  public function searchExecuted() {
    return !empty($this->keys);
  }

  /**
   * Returns a boolean flagging whether facets in a realm shoud be displayed.
   */
  public function suppressOutput($realm_name) {
    return FALSE;
  }

  /**
   * Overrides to the settings form to add some specific options
   */
  public function settingsForm(&$form, &$form_state) {
    $facet = $form['#facetapi']['facet'];
    $global_settings = $this->getFacet($facet)->getSettings();
    $form['global']['facet_calculation_post_query'] = array(
      '#type' => 'select',
      '#access' => true,
      '#title' => t('Facet calculation'),
      '#prefix' => '<div class="facetapi-global-setting">',
      '#suffix' => '</div>',
      '#field_suffix' => ' filtering query',
      '#options' => array(0 => t('before'), 1 => t('after')),
      '#default_value' => @$global_settings->settings['facet_calculation_post_query'],
      '#description' => t('Facet calculation may be based on result before or after filtering process')
    );

    if($facet['query type'] == 'date') {

      // date facet
      $form['global']['facet_date_interval'] = array(
        '#type' => 'fieldset',
        '#tree' => FALSE,
        '#title' => t('Date interval hierarchy')
      );

      // date facet
      $form['global']['facet_date_interval']['facet_date_interval_start'] = array(
        '#type' => 'select',
        '#access' => true,
        '#parents' => array('global', 'facet_date_interval_start'),
        '#title' => t('From'),
        '#prefix' => '<div class="facetapi-global-setting">',
        '#suffix' => '</div>',
        '#options' => array(
          FACETAPI_DATE_YEAR => t('Year'),
          FACETAPI_DATE_MONTH => t('Month'),
          FACETAPI_DATE_DAY => t('Day'),
          FACETAPI_DATE_HOUR => t('Hour'),
          FACETAPI_DATE_MINUTE => t('Minute')
        ),
        '#default_value' => @$global_settings->settings['facet_date_interval_start']
      );

      $form['global']['facet_date_interval']['facet_date_interval_end'] = array(
        '#type' => 'select',
        '#access' => true,
        '#parents' => array('global', 'facet_date_interval_end'),
        '#title' => t('to'),
        '#prefix' => '<div class="facetapi-global-setting">',
        '#suffix' => '</div>',
        '#options' => array(
          FACETAPI_DATE_YEAR => t('Year'),
          FACETAPI_DATE_MONTH => t('Month'),
          FACETAPI_DATE_DAY => t('Day'),
          FACETAPI_DATE_HOUR => t('Hour'),
          FACETAPI_DATE_MINUTE => t('Minute')
        ),
        '#default_value' => @$global_settings->settings['facet_date_interval_end']
      );
    }

  }

}
