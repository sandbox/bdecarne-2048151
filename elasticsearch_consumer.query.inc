<?php

/**
 * @file
 * ElasticsearchConsumerQuery
 */

/**
 * Implements the query object required by facetapi
 *
 * Responsibilities:
 * - handle the native search
 * - handler the addition of facets to the search
 */
class ElasticsearchConsumerQuery {

  /**
   * @var Elastica\Client
   */
  private $client;

  /**
   * @var array
   */
  private $indices;

  /**
   * @var string
   */
  private $keys;

  /**
   * @var Elastica\ResultSet
   */
  private $resultset;

  /**
   * @var Elastica\Query
   */
  private $query;

  /**
   * @var array
   */
  private $filters = array();

  /**
   * @var array
   */
  private $queryFilters = array();

  /**
   * @var array
   */
  private $highlightFields = array();

  /**
   * @var string
   */
  private $sortField = '_score';

  /**
   * @var string
   */
  private $sortOrder = 'desc';

  /**
   * Creates a query object
   */
  public function __construct($client, $indices, $keys = NULL) {
    $this->client = $client;
    $this->indices = $indices;
    $this->keys = $keys;
    $this->query = new Elastica\Query();
  }

  /**
   * @return \Elastica\Query
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * @return \Elastica\ResultSet
   */
  public function getResultset() {
    return $this->resultset;
  }

  /**
   * Add a facet
   */
  public function addFacet($facet) {
    $this->query->addFacet($facet);
  }

  /**
   * Add a filter
   */
  public function addFilter($filter, $query = FALSE) {
    if($query) {
      $this->addQueryFilter($filter);
    } else {
      $this->filters[] = $filter;
    }
  }

  /**
   * Add a query filter
   */
  public function addQueryFilter($filter) {
    $this->queryFilters[] = $filter;
  }

  /**
   * Add a highlight field
   */
  public function addHighlightField($field, $fragment_size = 120, $number_of_fragments = 3) {
    $this->highlightFields[$field] = array(
      'fragment_size' => $fragment_size,
      'number_of_fragments' => $number_of_fragments,
    );
  }

  /**
   * Get the highlight fields
   */
  public function getHighlightFields() {
    return $this->highlightFields;
  }

  /**
   * Get THE filter
   */
  public function getFilter() {
    if(count($this->filters)) {
      $filter = new \Elastica\Filter\BoolAnd();
      foreach($this->filters as $subFilter) {
        $filter->addFilter($subFilter);
      }
      return $filter;
    }
    return NULL;
  }

  /**
   * Get THE query filter
   */
  public function getQueryFilter() {
    if(count($this->queryFilters)) {
      $filter = new \Elastica\Filter\BoolAnd();
      foreach($this->queryFilters as $subFilter) {
        $filter->addFilter($subFilter);
      }
      return $filter;
    }
    return NULL;
  }

  /**
   * Set the pagination settings
   */
  public function setPagination($num_per_page = 10, $page = 0) {
    $this->query->setSize($num_per_page);
    $this->query->setFrom($num_per_page * $page);
  }

  /**
   * Add sort
   */
  public function setSort($field, $order = NULL) {
    $this->sortField = $field;
    $this->sortOrder = $order;
  }

  /**
   * Get sort field
   */
  public function getSortField() {
    return $this->sortField;
  }

  /**
   * Get sort order
   */
  public function getSortOrder() {
    return $this->sortOrder;
  }

  /**
   * Run the query
   */
  function execute() {
    // query string
    $query  = new \Elastica\Query\QueryString();
    $query->setDefaultOperator('AND');
    $query->setQuery($this->keys);

    // query
    if($queryFilter = $this->getQueryFilter()) {
      $query = new Elastica\Query\Filtered($query, $queryFilter);
    }
    $this->query->setQuery($query);

    // filter
    if($filter = $this->getFilter()) {
      $this->query->setFilter($filter);
    }

    // hightlights fields
    if($highlightFields = $this->getHighlightFields()) {
      $this->query->setHighlight(array(
          'pre_tags' => array('<strong>'),
          'post_tags' => array('</strong>'),
          'fields' => $highlightFields
      ));
    }

    // sort
    if($sortField = $this->getSortField()) {
      $param = $sortField;
      if($sortOrder = $this->getSortOrder()) {
        $param = array($sortField => array('order' => $sortOrder));
      }
      $this->query->addSort($param);
    }

    // allow other modules to alter the query
    $indices = array_values($this->indices);
    drupal_alter('elasticsearch_consumer_query', $this->query, $query, $indices);
    $this->query->setQuery($query);

    // search
    $search = new \Elastica\Search($this->client);
    $search->addIndices($indices);

    try {
      $this->resultset = $search->search($this->query);
    } catch(\Exception $e) {
      drupal_set_message(t('Search is temporarily unavailable. If the problem persists, please contact the site administrator.'), "error");
      watchdog('error', $e->getMessage());
      return NULL;
    }

    return $this->resultset;
  }
}
