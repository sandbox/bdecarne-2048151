<?php

/**
 * @file
 * Hooks provided by Elasticsearch consumer module.
 */

/**
 * Allows for alterations to the search query
 *
 * @param Elastica\Query &$elasticaQuery
 *   The elastica query object
 * @param AbstractQuery &$query
 *   The query object
 * @param array &$indices
 *   The indices the query will request
 *
 * @see ElasticsearchConsumerQuery::execute()
 */
function hook_elasticsearch_consumer_query_alter(&$elasticaQuery, &$query, &$indices) {

}

/**
 * Allows for add extra informations to the result
 *
 * @param array $result
 *   The initial result array
 * @return array
 *
 * @see elasticsearch_consumer_search_execute()
 */
function hook_elasticsearch_consumer_search_result($result) {
  return array(
    'mykey' => 'mydata'
  );
}

/**
 * Allows for add order options
 *
 * @return array
 *
 * @see elasticsearch_consumer_search_execute()
 */
function hook_elasticsearch_consumer_order_options() {
  return array(
    'my_custom_field' => array(     // elasticsearch field name
      'label' => t('My Field'),     // label of the option
      'order' => 'asc',             // default order
      'toggle' => false             // toggle on click again
    )
  );
}

/**
 * Allows for alterations to the order options
 *
 * @param array $options
 *   The initial options array
 *
 * @see elasticsearch_consumer_search_execute()
 */
function hook_elasticsearch_consumer_order_options_alter(&$options) {
  unset($options['my_custom_field']);
}
